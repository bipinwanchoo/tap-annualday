#Annual Day Ideas go here

Theme : Gentleman - The Stereotypes

#IDEAS and Tasks

1) PPTs in sync with other verticals

2) Questionnaire based if else interactive questions (In each of these questions, we can put a Yes/No button upon clicking which we reveal if user has answered correctly or incorrectly including a picture in the background:
 - Game title - Time is Ripe, Wipe the stereotype!
 - Kya ek 'mard' ke liye 'Rona' uski mardangi ko kam kar deta hai. A small 2-3 line story context with a picture ending in this question
 - Kya 'ghar' ke kaam karne se mard ki mardangi pe farak padta hai?
 - Kya ek couple ko ek relationship me bolne ka barabar haq hona chahiye?
 - Kya family ke responsibility sirf ladko ke hi lene chahiye ya ladkiyon ko b barabar haqq milna chahiye?
 - 
 
 3) Space Invader Game (Where we can potray space invaders as stereotypes)

 - We will design the UI, but backend code will be done by children.
  Source Code : https://www.youtube.com/watch?v=FfWpgLFMI7w
  Source Images : Flaticon.com
  
  Add pygame to conda using this command from cmd : conda install --channel https://conda.anaconda.org/CogSci pygame